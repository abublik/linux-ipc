#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>

const char *memname = "/xxx_memory";

char *open_shared_memory() {
  char *shared_mem = NULL;
  long region_size = sysconf(_SC_PAGESIZE);

  int fd = shm_open(memname, O_RDWR, 0);
  if (fd == -1) {
    printf("shm_open failed\n");
  }
  
  int protection = PROT_READ | PROT_WRITE;

  // The buffer will be shared (meaning other processes can access it), but
  // anonymous (meaning third-party processes cannot obtain an address for it),
  // so only this process and its children will be able to use it:
  //int visibility = MAP_ANONYMOUS | MAP_SHARED;
  int visibility = MAP_SHARED;

  shared_mem = mmap(NULL, region_size, protection, visibility, fd, 0);
  if (shared_mem == MAP_FAILED) {
    printf("mmap failed\n");
  }
  return shared_mem;
}


int main(int argc, char *argv[]) {
  char *shmem = open_shared_memory();
  if (shmem) {
    char *parent_msg = "ales";
    printf("PREVISOU SHMEM content: %s\n", (char *) shmem);
    memcpy(shmem, parent_msg, sizeof(parent_msg));
    printf("CURRENT SHMEM content: %s\n", (char *) shmem);
  }  else {
    return 1;
  }
  return 0;
}
