#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>

void* create_shared_memory(size_t size) {
	int protection = PROT_READ | PROT_WRITE;

    // The buffer will be shared (meaning other processes can access it), but
    // anonymous (meaning third-party processes cannot obtain an address for it),
    // so only this process and its children will be able to use it:
	int visibility = MAP_ANONYMOUS | MAP_SHARED;

	return mmap(NULL, size, protection, visibility, 0, 0);
}

int main(int argc, char *argv[])
{
	char *parent_msg = "hello";
	char *child_msg = "goodbye";

	void *shmem = create_shared_memory(128);

	memcpy(shmem, parent_msg, sizeof(parent_msg));

	int pid = fork();

	if (pid == 0) {
		printf("Child read: %s\n", shmem);
		memcpy(shmem, child_msg, sizeof(child_msg));
		printf("Child wrote: %s\n", shmem);
	} else {
		printf("Parent read: %s\n", shmem);
		sleep(1);
		printf("After 1s, parent read: %s\n", shmem);
	}
	return 0;
}
