#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	const char *memname = "/mem";
        int r;
	const size_t region_size = getpagesize();

	int fd = shm_open(memname, O_RDWR | O_CREAT, 0660);
	if (fd == -1) {
		printf("shm_open failed\n");
	}

	r = ftruncate(fd, region_size);
	if (r != 0) {
		printf("ftruncate\n");
	}

	void *ptr = mmap(NULL, region_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (ptr == MAP_FAILED) {
		printf("map failed\n");
	}
	close(fd);


	pid_t pid = fork();

	if (pid == 0) {
                printf("CHILD\n");
		u_long *d = (u_long *) ptr;
		*d = 0xdbeebee;
		exit(0);
	} else {
                printf("PARENT\n");
		int status;
		waitpid(pid, &status, 0);
		printf("child wrote %#lx\n", *(u_long *) ptr);
	}

	printf("munmap\n");
	r = munmap(ptr, region_size);
	if (r != 0) {
		printf("munmap\n");
	}

	printf("shm_unlink\n");
	r = shm_unlink(memname);
	if (r != 0) {
		printf("shm_unlink\n");
	}

	return 0;
}
