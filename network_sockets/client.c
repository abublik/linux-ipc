#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>

#define SERVER_PORT 12345

void timestamp() {
    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    printf("%s", asctime(localtime(&ltime)));
}


int main(int argc, char *argv[]) {
	int client_sd;
	int rc;
	char send_buffer[256];
	char recv_buffer[256];
	struct sockaddr_in client_addr;

	//
	FILE *fp;
	char *buffer;
	long filelen;
	fp = fopen(argv[1], "rb");
	fseek(fp, 0, SEEK_END);
	filelen = ftell(fp);
	rewind(fp);

	buffer = (char *)malloc((filelen+1)*sizeof(char));
	fread(buffer, filelen, 1, fp);
	fclose(fp);

	FILE *write_fp;
	write_fp = fopen("test.iso", "wb");
	//
	
	// socket
	client_sd = socket(AF_INET, SOCK_STREAM, 0);
	if (client_sd == -1) {
		printf("socket creation failed\n");
		exit(1);
	}

	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	client_addr.sin_port = htons(SERVER_PORT);

	// connect
	rc = connect(client_sd, (struct sockaddr *)&client_addr, sizeof(struct sockaddr_in));
	if (rc == -1) {
		printf("connect failed\n");
		close(client_sd);
		exit(1);
	}

	printf("Connected\n");

	printf("Enter data to be send:\n");
	gets(send_buffer);

	// send
	unsigned int len = send(client_sd, send_buffer, strlen(send_buffer) + 1, 0);
	if (len != strlen(send_buffer) + 1) {
		printf("send\n");
		close(client_sd);
		exit(1);
	}
	printf("%d bytes sent\n", len);

	// receive
	rc = recv(client_sd, buffer, sizeof(buffer), 0);
	if (rc == -1) {
		printf("recv failed\n");
		close(client_sd);
		exit(1);
	} else {
		timestamp();
		fwrite(buffer, (filelen+1)*sizeof(char), 1, write_fp);
		timestamp();
	}

	printf("%d bytes received\n", len);

	close(client_sd);
	return 0;
}
