#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>

#define SERVER_PORT 12345

void timestamp() {
    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    printf("%s", asctime(localtime(&ltime)));
}


int main(int argc, char *argv[])
{
	int num = 1;
	int listen_sd;
	int rc;
	int accept_sd;
	int on = 1;
	char buffer[256];
	struct sockaddr_in server_addr;

	//
	FILE *fp;
	char *data_buffer;
	long filelen;
	fp = fopen(argv[1], "rb");
	fseek(fp, 0, SEEK_END);
	filelen = ftell(fp);
	rewind(fp);
	data_buffer = (char *)malloc((filelen+1)*sizeof(char));
	fread(data_buffer, filelen, 1, fp);
	fclose(fp);
	//

	// socket
	listen_sd = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_sd == -1) {
		printf("socket() failed\n");
		exit(1);
	}

	rc = setsockopt(listen_sd,
			        SOL_SOCKET, SO_REUSEADDR,
					(char *)&on, sizeof(on));
	if (rc == -1) {
		printf("setsockopt failed\n");
		close(listen_sd);
		exit(1);
	}

	// bind
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(SERVER_PORT);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	rc = bind(listen_sd, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (rc == -1) {
		printf("bind failed");
		close(listen_sd);
		exit(1);
	}

	// listen
	rc = listen(listen_sd, 5);
	if (rc == -1) {
		printf("listen failed");
		close(listen_sd);
		exit(1);
	}

	// accept
	printf("Listening\n");
	for (int i = 0; i < num; i++) {
		printf(" waiting on accept\n");
		accept_sd = accept(listen_sd, NULL, NULL);
		if (accept_sd == -1) {
			printf("accept failed\n");
			close(listen_sd);
			exit(1);
		}

		printf(" accept completed successfully\n");

		// receive
		printf(" wait for client to send a message\n");
		rc = recv(accept_sd, buffer, sizeof(buffer), 0);
		if (rc == -1) {
			printf("recv failed\n");
			close(listen_sd);
			close(accept_sd);
			exit(1);
		}
		printf(" %s\n", buffer);

		// echo
		printf(" echo it back\n");
		int len = rc;
		timestamp();
		rc = send(accept_sd, data_buffer, sizeof(data_buffer), 0);
		if (rc == -1) {
			printf(" send failed\n");
			close(listen_sd);
			close(accept_sd);
			exit(1);
		} else {
			printf("Data sent\n");
		}

		close(accept_sd);
	}


	close(listen_sd);
	return 0;
}
