#include <stdio.h>
#include <unistd.h>
#include <string.h>

// "read" end of the pipe
#define READ 0

// "write" end of the pipe
#define WRITE 1

char *phrase = "Stuff this in your pipe and smoke it";

int main(int argc, char *argv[])
{
	int fd[2], bytesRead;
	char message[100];

	// create unnamed pipe
	int rs = pipe(fd);
	if (rs == -1) {
		printf("Pipe creation error\n");
	}

	if (fork() == 0) {
		// Child writer
		close(fd[READ]);
		write(fd[WRITE], phrase, strlen(phrase) +1); // include NULL
		close(fd[WRITE]); // close the end
		printf("Child: Wrote %s to pipe!\n", phrase);
	} else {
		// Parent reader
		close(fd[WRITE]); // close unused end
		bytesRead = read(fd[READ], message, 100);
		printf("Parent: Read %d bytes from pipe: %s\n", bytesRead, message);
		close(fd[READ]);
	}
	return 0;
}
