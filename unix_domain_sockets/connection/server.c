#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <time.h>

#define SOCK_PATH "server.socket"
#define DATA "Hello from server"

void timestamp() {
    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    printf("%s", asctime(localtime(&ltime)));
}


int main(int argc, char *argv[])
{
    int server_sock, client_sock;
    int bytes_rec = 0;

	//
	FILE *fp;
	char *buffer;
	long filelen;
	fp = fopen(argv[1], "rb");
	fseek(fp, 0, SEEK_END);
	filelen = ftell(fp);
	rewind(fp);

	buffer = (char *)malloc((filelen+1)*sizeof(char));
	fread(buffer, filelen, 1, fp);
	fclose(fp);
	//

    struct sockaddr_un server_addr;
    memset(&server_addr, 0, sizeof(struct sockaddr_un));

    struct sockaddr_un client_addr;
    memset(&client_addr, 0, sizeof(struct sockaddr_un));

    char data[1024];
    memset(&data, 0, 1024);

    int backlog = 10;

    // create socket
    server_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (server_sock == -1) {
        printf("SOCKET ERROR\n");
        exit(1);
    }

    // set up UNIX sockaddr structure
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, SOCK_PATH);

    int len = sizeof(server_addr);
    unlink(SOCK_PATH);

    // bind
    int rc = bind(server_sock, (struct sockaddr *) &server_addr, len);
    if (rc == -1) {
        printf("BIND ERROR\n");
        close(server_sock);
        exit(1);
    }

    // listen
    rc = listen(server_sock, backlog);
    if (rc == -1) {
        printf("LISTEN ERROR\n");
        close(server_sock);
        exit(1);
    }

    printf("socket listening...\n");

    // accept
    client_sock = accept(server_sock, (struct sockaddr *) &client_addr, (socklen_t *)&len);
    if (client_sock == -1) {
        printf("ACCEPT ERROR\n");
        close(server_sock);
        close(client_sock);
        exit(1);
    }

    // getpeername
    len = sizeof(client_addr);
    rc = getpeername(client_sock, (struct sockaddr *) &client_addr, (socklen_t *)&len);
    if (rc == -1) {
        printf("GETPEERNAME ERROR\n");
        close(server_sock);
        close(client_sock);
        exit(1);
    } else {
        printf("Client socket filepath: %s\n", client_addr.sun_path);
    }

    // read data
    printf("waiting to read ...\n");
    bytes_rec = recv(client_sock, data, sizeof(data), 0);
    if (bytes_rec == -1) {
        printf("RECV ERROR\n");
        close(server_sock);
        close(client_sock);
        exit(1);
    } else {
        printf("DATA RECEIVED = %s\n", data);
    }

    // send
	printf("Sending data...\n");
	timestamp();
	rc = send(client_sock, buffer, sizeof(buffer), 0);
	if (rc == -1) {
		printf("SEND ERROR\n");
		close(server_sock);
		close(client_sock);
		exit(1);
	} else {
		printf("Data sent!\n");
	}

    // close
    close(server_sock);
    close(client_sock);

    return 0;
}
