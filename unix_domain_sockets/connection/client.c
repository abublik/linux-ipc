#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <time.h>

#define SERVER_PATH "server.socket"
#define CLIENT_PATH "client.socket"
#define DATA "Hello from client"

void timestamp() {
    time_t ltime; /* calendar time */
    ltime=time(NULL); /* get current cal time */
    printf("%s", asctime(localtime(&ltime)));
}


int main(int argc, char *argv[])
{
    struct sockaddr_un server_addr;
    struct sockaddr_un client_addr;
    char data[1024];

    memset(&server_addr, 0, sizeof(struct sockaddr_un));
    memset(&client_addr, 0, sizeof(struct sockaddr_un));

	FILE *fp;
	char *buffer;
	long filelen;
	fp = fopen(argv[1], "rb");
	fseek(fp, 0, SEEK_END);
	filelen = ftell(fp);
	rewind(fp);

	buffer = (char *)malloc((filelen+1)*sizeof(char));
	fread(buffer, filelen, 1, fp);
	fclose(fp);
	
	FILE *write_fp;
	write_fp = fopen("test.iso", "wb");

    // create socket
    int client_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (client_sock == -1) {
        printf("SOCKET ERROR\n");
        exit(1);
    }

    client_addr.sun_family = AF_UNIX;
    strcpy(client_addr.sun_path, CLIENT_PATH);
    int len = sizeof(client_addr);

    unlink(CLIENT_PATH);

    // bind
    int rc = bind(client_sock, (struct sockaddr *) &client_addr, len);
    if (rc == -1) {
        printf("BIND ERROR\n");
        close(client_sock);
        exit(1);
    }

    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, SERVER_PATH);

    // connect
    rc = connect(client_sock, (struct sockaddr *) &server_addr, len);
    if (rc == -1) {
        printf("CONNECT ERROR\n");
        close(client_sock);
        exit(1);
    }

    // send
    strcpy(data, DATA);
    printf("Sending data...\n");
    rc = send(client_sock, data, strlen(data), 0);
    if (rc == -1) {
        printf("SEND ERROR\n");
        close(client_sock);
        exit(1);
    } else {
        printf("Data sent!\n");
    }

    // read
	//printf("Waiting to receive data...\n");
	//memset(buffer, 0, sizeof(buffer));
	// (filelen+1)*sizeof(char)
	rc = recv(client_sock, buffer, (filelen+1)*sizeof(char), 0);
	if (rc == -1) {
		printf("RECV ERROR\n");
		close(client_sock);
		exit(1);
	} else {
		timestamp();
		fwrite(buffer, (filelen+1)*sizeof(char), 1, write_fp);
		//printf("%d\n", counter);
		//printf("DATA RECEIVED = %s\n", data);
	}

    // close
    close(client_sock);

    return 0;
}
